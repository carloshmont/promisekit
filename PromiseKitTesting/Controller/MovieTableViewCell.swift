//
//  MovieTableViewCell.swift
//  PromiseKitTesting
//
//  Created by Carlos H Montenegro on 6/22/17.
//  Copyright © 2017 Testing. All rights reserved.
//

import UIKit
import SDWebImage

protocol MovieTableViewCellDelegate : class {
    func tableViewCell(didTapDeleteButtonOn cell: MovieTableViewCell)
}


class MovieTableViewCell: UITableViewCell {

    var delegate : MovieTableViewCellDelegate?
    
    var title : String? {
        didSet {
            movieTitleLabel.text = title
        }
    }
    
    var rating : Double? {
        didSet {
            guard let thisRating = rating else {
                movieRatingLabel.text = "Not Rated"
                return
            }
            movieRatingLabel.text = "Rating: \(thisRating)"
        }
    }
    
    var poster : URL? {
        didSet {
            guard let thisPoster = poster else {
                return
            }
            moviePosterImageView.sd_setImage(with: thisPoster)
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        guard let delegateRef = delegate else { return }
        delegateRef.tableViewCell(didTapDeleteButtonOn: self)
    }
    
    @IBOutlet fileprivate weak var movieTitleLabel: UILabel!
    @IBOutlet fileprivate weak var movieRatingLabel: UILabel!
    @IBOutlet fileprivate weak var moviePosterImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        moviePosterImageView.layer.cornerRadius = moviePosterImageView.bounds.height / 2
        moviePosterImageView.layer.masksToBounds = true
        
    }

}
