//
//  ViewController.swift
//  PromiseKitTesting
//
//  Created by Carlos H Montenegro on 6/21/17.
//  Copyright © 2017 Testing. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    fileprivate var movieViewModel = MovieViewModel()
    
    @IBOutlet fileprivate weak var movieListTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }

    @IBAction func reloadButtonTapped(_ sender: UIBarButtonItem) {
        loadMovies()
    }
}

extension ViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieViewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let newCell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as! MovieTableViewCell
        
        newCell.title  = movieViewModel.getMovieTitle(atIndex: indexPath.row)
        newCell.rating = movieViewModel.getMovieRating(atIndex: indexPath.row)
        newCell.poster = movieViewModel.getMoviePoster(atIndex: indexPath.row)
        
        newCell.delegate = self
        
        return newCell
        
    }
    
}

extension ViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

extension ViewController : MovieTableViewCellDelegate {
    
    func tableViewCell(didTapDeleteButtonOn cell: MovieTableViewCell) {
        guard let index = movieListTableView.indexPath(for: cell) else { return }
        
        if movieViewModel.removeMovie(atIndex: index.row) {
            movieListTableView.deleteRows(at: [index], with: .fade)
        } else {
            print("Element failed being removed")
        }
        
    }
    
}

extension ViewController {
    
    fileprivate func initialSetup() {
        setupTableViewController()
        loadMovies()
    }
    
    fileprivate func loadMovies() {
    
        MovieService.getTarantinoMovies { (movies, error) in
            if let failure = error {
                print(failure.localizedDescription)
            } else {
                guard let thisMovies = movies else { return }
                self.movieViewModel.movieList = thisMovies
                self.movieListTableView.reloadData()
            }
        }
        
        
    }
    
    fileprivate func setupTableViewController() {
        movieListTableView.delegate = self
        movieListTableView.dataSource = self
    }
    
}

