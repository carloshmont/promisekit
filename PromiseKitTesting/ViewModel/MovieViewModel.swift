//
//  MovieViewModel.swift
//  PromiseKitTesting
//
//  Created by Carlos H Montenegro on 6/26/17.
//  Copyright © 2017 Testing. All rights reserved.
//

import Foundation

protocol MovieViewModelDelegate: class {
    
    func didChange(dataIn movieViewModel: MovieViewModel)
    
}

class MovieViewModel {
    
    weak var delegate : MovieViewModelDelegate?
    
    var movieList : [Movie]? {
        didSet {
            guard let delegateRef = delegate, let _ = movieList else { return }
            delegateRef.didChange(dataIn: self)
        }
    }
    
    var count : Int {
        guard let list = movieList else { return 0 }
        return list.count
    }
    
    required init() {
        
    }
    
    convenience init(movieList: [Movie]) {
        self.init()
        self.movieList = movieList
    }
    
}

extension MovieViewModel {
    
    func getMovieTitle(atIndex index: Int) -> String? {
        guard let list = movieList, index < list.count else { return nil }
        return list[index].showTitle
    }
    
    func getMovieRating(atIndex index: Int) -> Double? {
        guard let list = movieList, index < list.count else { return nil }
        return list[index].rating
    }
    
    func getMoviePoster(atIndex index: Int) -> URL? {
        guard let list = movieList, index < list.count else { return nil }
        return list[index].poster
    }
    
}

extension MovieViewModel {
    
    func removeMovie(atIndex index: Int) -> Bool {
        guard let list = movieList, index < list.count else { return false }
        movieList?.remove(at: index)
        return true
    }
    
}
