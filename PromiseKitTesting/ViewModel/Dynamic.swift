//
//  Dynamic.swift
//  PromiseKitTesting
//
//  Created by Carlos H Montenegro on 6/28/17.
//  Copyright © 2017 Testing. All rights reserved.
//

import Foundation

class Dynamic<T> {

    typealias Listener = (T) -> Void
    
    var listener : Listener?
    
    func bind(_ listener: Listener?) {
        self.listener = listener
    }
    
    func bindAndFire(_ listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ v: T) {
        value = v
    }
    
}
