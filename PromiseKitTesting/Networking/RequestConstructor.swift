//
//  RequestConstructor.swift
//
//  Created by Carlos H Montenegro on 6/23/17.
//

import Foundation

class RequestBuilder {
    
    //TODO: - Replace path with string from environment variables after setting up Schemes for each one
    
    private struct UrlStructure {
        static let scheme = "http://"
        static let host   = "netflixroulette.net"
        static let port   = ""
        static let path   = "/api/api.php"
    }
    
    private static let baseURL = URL(string: "\(UrlStructure.scheme)\(UrlStructure.host)\(UrlStructure.port)\(UrlStructure.path)?director=Quentin%20Tarantino")
    
    private static let headerFields = ["Content-Type" : "application/json", "Accept" : "application/json"]
    
    fileprivate enum Method : String {
        case get    = "GET"
        case post   = "POST"
        case put    = "PUT"
        case delete = "DELETE"
        case head   = "HEAD"
    }
    
    fileprivate static func buildRequest(withBody body: Any?, httpMethod: String, andAuth auth: Bool) -> URLRequest? {
        guard let url = baseURL else { return nil }
        var newRequest = URLRequest(url: url)
        
        newRequest.httpMethod = httpMethod
        
        // Adding headers for request
        
        for field in headerFields {
            newRequest.addValue(field.value, forHTTPHeaderField: field.key)
        }
        
        // Adding authorization if necessary
        
        if auth {
            newRequest.addValue("", forHTTPHeaderField: "Authorization")
        }
        
        // Adding request body if needed
        
        if let requestBody = body {
            guard let jsonData = try? JSONSerialization.data(withJSONObject: requestBody, options: .prettyPrinted) else { return nil }
            newRequest.httpBody = jsonData
        }
        
        return newRequest
    }
    
}

//MARK: - HTTP Methods

extension RequestBuilder {
    
    static func get(withBody body: Any?, andAuthorization auth: Bool) -> URLRequest? {
        return buildRequest(withBody: body, httpMethod: Method.get.rawValue, andAuth: auth)
    }
    
    static func post(withBody body: Any?, andAuthorization auth: Bool) -> URLRequest? {
        return buildRequest(withBody: body, httpMethod: Method.post.rawValue, andAuth: auth)
    }
    
    static func put(withBody body: Any?, andAuthorization auth: Bool) -> URLRequest? {
        return buildRequest(withBody: body, httpMethod: Method.post.rawValue, andAuth: auth)
    }
    
    static func delete(withBody body: Any?, andAuthorization auth: Bool) -> URLRequest? {
        return buildRequest(withBody: body, httpMethod: Method.delete.rawValue, andAuth: auth)
    }
    
    static func head(withBody body: Any?, andAuthorization auth: Bool) -> URLRequest? {
        return buildRequest(withBody: body, httpMethod: Method.head.rawValue, andAuth: auth)
    }
    
}
