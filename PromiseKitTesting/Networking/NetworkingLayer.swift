//
//  NetworkingLayer.swift
//  PromiseKitTesting
//
//  Created by Carlos H Montenegro on 6/21/17.
//  Copyright © 2017 Testing. All rights reserved.
//

import Foundation
import PromiseKit

class NetworkingLayer {
    
    // Using the classic way to make a Networking call
    
    // We use data tasks from URLSession
    
    static func getAllMovies(completion: @escaping ([Movie]?, Error?)->()) {
        guard let request = RequestBuilder.get(withBody: nil, andAuthorization: false) else { return }
        
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let failure = error {
                completion(nil, failure)
            } else {
                guard let thisData = data, let jsonObject = try? JSONSerialization.jsonObject(with: thisData, options: []) else { return }
                let movieList = parseJson(json: jsonObject as! [Any])
                completion(movieList, nil)
            }
        }
        dataTask.resume()
    }
    
    // Using PromiseKit
    
    // We wrap the networking into a Promise
    
    static func getAllTarantinoMovies() -> Promise<[Movie]> {
        
        return Promise {
            fulfill, reject in
            guard let request = RequestBuilder.get(withBody: nil, andAuthorization: false) else { return }
            
            let dataPromise : URLDataPromise = URLSession.shared.dataTask(with: request)
            
            _ = dataPromise.asArray().then { array -> Void in
                let movieList = parseJson(json: array as! [Any])
                fulfill(movieList)
            }.catch(execute: reject)
            
        }
    }
    
}

//MARK: - Parsing Auxiliary Functions

extension NetworkingLayer {
    
    fileprivate static func parseJson(json: [Any]) -> [Movie] {
        var movieList = [Movie]()
        _ = json.map { movies in movieList.append(Movie(json: movies as! [String: Any])) }
        
        return movieList
    }
    
}
