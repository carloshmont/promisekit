//
//  Movie.swift
//  PromiseKitTesting
//
//  Created by Carlos H Montenegro on 6/21/17.
//  Copyright © 2017 Testing. All rights reserved.
//

import Foundation
import UIKit

class Movie: NSObject {
    
    var unit        : Int!
    var showId      : Int!
    var showTitle   : String!
    var releaseYear : String!
    var rating      : Double!
    var category    : String!
    var cast        : [String]!
    var director    : String!
    var summary     : String!
    var poster      : URL!
    var runtime     : String!
    
    override init() {
        super.init()
    }
    
    convenience init(unit: Int, showId: Int, showTitle: String, releaseYear: String, rating: Double, category: String, cast: [String], director: String, summary: String, poster: URL, runtime: String) {
        self.init()
        
        self.unit = unit
        self.showId = showId
        self.showTitle = showTitle
        self.releaseYear = releaseYear
        self.rating = rating
        self.category = category
        self.cast = cast
        self.director = director
        self.summary = summary
        self.poster = poster
        self.runtime = runtime
        
    }
    
    convenience init(json: [String: Any]) {
        self.init()
        
        guard let thisUnit = json[JsonKeys.unit] as? Int, let thisShowId = json[JsonKeys.showId] as? Int,
              let thisShowTitle = json[JsonKeys.showTitle] as? String, let thisReleaseYear = json[JsonKeys.releaseYear] as? String,
              let thisRating = json[JsonKeys.rating] as? String, let thisCategory = json[JsonKeys.category] as? String,
              let thisCast = json[JsonKeys.showCast] as? String, let thisDirector = json[JsonKeys.director] as? String,
              let thisSummary = json[JsonKeys.summary] as? String, let thisPoster = json[JsonKeys.poster] as? String,
              let thisRuntime = json[JsonKeys.runtime] as? String
        else { return }
        
        self.unit   = thisUnit
        self.showId = thisShowId
        self.showTitle   = thisShowTitle
        self.releaseYear = thisReleaseYear
        self.rating      = Double(thisRating)
        self.category    = thisCategory
        self.cast        = thisCast.components(separatedBy: ",")
        self.director    = thisDirector
        self.summary     = thisSummary
        self.poster      = URL(string: thisPoster)
        self.runtime     = thisRuntime
        
    }

}


extension Movie {
    
    fileprivate struct JsonKeys {
        static let unit        = "unit"
        static let showId      = "show_id"
        static let showTitle   = "show_title"
        static let releaseYear = "release_year"
        static let rating      = "rating"
        static let category    = "category"
        static let showCast    = "show_cast"
        static let director    = "director"
        static let summary     = "summary"
        static let poster      = "poster"
        static let mediaType   = "mediatype"
        static let runtime     = "runtime"
    }
    
    fileprivate func stringArray(fromString string: String) -> [String] {
        return string.components(separatedBy: ",")
    }
    
}
