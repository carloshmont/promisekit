//
//  MovieService.swift
//  PromiseKitTesting
//
//  Created by Carlos H Montenegro on 6/26/17.
//  Copyright © 2017 Testing. All rights reserved.
//

import UIKit

class MovieService {
    
    static func getTarantinoMovies(completion: @escaping ([Movie]?, Error?)->Void) {
        NetworkingLayer.getAllTarantinoMovies().then {
            movies -> Void in
            completion(movies, nil)
        }.catch { error in
            completion(nil, error)
        }
    }
    
}
